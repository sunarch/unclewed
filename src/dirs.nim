# Unclewed - a Clew DB-dump analysis tool
# Copyright (C) 2024  András Németh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from std/os import dirExists, fileExists
from std/paths import Path, `/`
from std/strformat import fmt


func compose_2*(part_1: string, part_2: string): string =
    return (part_1.Path / part_2.Path).string


proc compose_and_check_dir*(prefix: string, dir: string, default_dir: string): string =

    if prefix == "":
        if dir == "":
            result = default_dir
        else:
            result = dir
    else:
        if not dirExists(prefix):
            quit(fmt"Given dir prefix does not exist: '{prefix}'", QuitFailure)

        if dir == "":
            result = compose_2(prefix, default_dir)
        else:
            result = compose_2(prefix, dir)

    if not dirExists(result):
        quit(fmt"Given dir does not exist: '{result}'", QuitFailure)


proc compose_and_check_path*(dir: string, filename: string, default_filename: string): string =

    if filename == "":
        echo(fmt"No filename given, using default: {default_filename}")
        result = compose_2(dir, default_filename)
    else:
        result = compose_2(dir, filename)

    if not (fileExists(result)):
        quit(fmt"File with the given path does not exist: '{result}'", QuitFailure)
