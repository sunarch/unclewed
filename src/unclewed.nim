# Unclewed - a Clew DB-dump analysis tool
# Copyright (C) 2024  András Németh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import std/parseopt as po
from std/strformat import fmt

# project imports
import version as version
import common as common
import sections as sections
when defined(DEBUG):
    import debug as debug


proc show_help =
    echo(version.long())
    echo(version.compiled())
    echo(version.copyright())
    echo()
    echo(fmt"    {version.ProgramName} [options]")
    echo()
    echo("Stage 1 (break up DB-dump):")
    echo("  --stage-1     Run stage 1")
    echo("  --sections    Alias to run stage 1")
    echo("  --filename    Set filename of dump")
    echo()
    echo("Options:")
    echo("  --dir-prefix  Set directory prefix for paths")
    echo("  --srcdir      Set source directory")
    echo("  --outdir      Set output directory")
    echo()
    echo("Options for direct output:")
    echo("  --help        Show this help and exit")
    echo("  --version     Show version information and exit")
    quit(QuitSuccess)


type Stage = enum
    stageNone = 0
    stageSections = 1

type
    Options = ref OptionsObj
    OptionsObj = object
        stage: Stage = stageNone


const OptionsLongNoVal = @[
    "help",
    "version",
]


proc main =

    var p = po.initOptParser(shortNoVal = {}, longNoVal = OptionsLongNoVal)

    when defined(DEBUG):
        var p_debug = p
        debug.output_options(p_debug)

    var options: Options
    new(options)

    var options_common: common.Options
    new(options_common)

    var options_stage_1: sections.Options
    new(options_stage_1)

    while true:
        p.next()
        case p.kind
            of po.cmdEnd:
                break
            of po.cmdShortOption, po.cmdLongOption:
                if p.key in OptionsLongNoVal and p.val != "":
                    quit(fmt"Command line option '{p.key}' doesn't take a value", QuitFailure)
                case p.key:
                    # Options for direct output:
                    of "help":
                        show_help()
                        return
                    of "version":
                        echo(version.long())
                        return
                    of "dir-prefix":
                        options_common.dir_prefix = p.val
                    of "srcdir":
                        options_common.source_dir = p.val
                    of "outdir":
                        options_common.output_dir = p.val
                    of "stage-1", "sections":
                        options.stage = stageSections
                    of "filename":
                        options_stage_1.filename = p.val
                    else:
                        quit(fmt"Unrecognized command line option '{p.key}'", QuitFailure)
            of po.cmdArgument:
                quit(fmt"This program doesn't take any non-option arguments: '{p.key}'", QuitFailure)

    case options.stage:
        of stageSections:
            sections.run(options_common, options_stage_1)
        else:
            quit(fmt"Unrecognized stage: '{options.stage}'", QuitFailure)


when isMainModule:
    main()
