# Unclewed - a Clew DB-dump analysis tool
# Copyright (C) 2024  András Németh
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from std/streams import FileStream, newFileStream, close, readLine
from std/strformat import fmt
from std/strscans import scanf
from std/strutils import strip

# project imports
from common import dual_output
import dirs as dirs
import version as version


type
    Options* = ref OptionsObj
    OptionsObj = object
        filename* = ""

type
    Section = ref SectionObj
    SectionObj = object
        name_prefix*: string
        name*: string
        `type`*: string
        schema*: string
        owner*: string

        first_line*: uint32
        last_line*: uint32


const
    SourceDefaultSubdir = "stage-0"
    SourceDefaultFilename = "clew-index.dump"

const
    Subdir = "stage-1"
    ResultFilename = "stats-db.txt"


proc print_header(section: Section, stream: FileStream) =
    dual_output(fmt"{section.name_prefix}Name: {section.name}", stream)
    dual_output(fmt"    Type:   {section.type}", stream)
    dual_output(fmt"    Schema: {section.schema}", stream)
    dual_output(fmt"    Owner:  {section.owner}", stream)


proc print_stats(section: Section, stream: FileStream) =
    dual_output(fmt"    {section.first_line} - {section.last_line} ({section.last_line - section.first_line})", stream)


proc run*(options_common: common.Options, options: Options) =

    let source_dir = dirs.compose_and_check_dir(options_common.dir_prefix,
                                                options_common.source_dir,
                                                SourceDefaultSubdir)
    let output_dir = dirs.compose_and_check_dir(options_common.dir_prefix,
                                                options_common.output_dir,
                                                Subdir)

    let source_filepath = dirs.compose_and_check_path(source_dir, options.filename, SourceDefaultFilename)
    let filepath_stats_db = dirs.compose_2(output_dir, ResultFilename)

    var fh_input = newFileStream(source_filepath, fmRead)
    defer: fh_input.close()
    if isNil(fh_input):
        quit(fmt"Unable to open input file: '{source_filepath}'", QuitFailure)

    var fh_stats_db = newFileStream(filepath_stats_db, fmWrite)
    defer: fh_stats_db.close()
    if isNil(fh_stats_db):
        quit(fmt"Unable to open output file: '{filepath_stats_db}'", QuitFailure)

    const Header1 = " Data processing by: "
    dual_output(fmt"{Header1:=^80}", fh_stats_db)
    dual_output(version.long(), fh_stats_db)
    dual_output(version.compiled(), fh_stats_db)
    const Header2 = " Processed file "
    dual_output(fmt"{Header2:=^80}", fh_stats_db)
    dual_output(source_filepath, fh_stats_db)
    const Header3 = " Sections "
    dual_output(fmt"{Header3:=^80}", fh_stats_db)

    var
        line_count: uint32 = 0
        section_count: uint16 = 0

    var section: Section
    new(section)

    var line = ""
    var in_section_title = false
    while fh_input.readLine(line):
        line_count += 1
        if line == "--":
            in_section_title = not in_section_title
        else:
            if in_section_title:
                section.last_line = line_count - 2
                section.print_stats(fh_stats_db)
                section.first_line = line_count - 1
                section_count += 1
                if scanf(line, "-- $*Name: $+; Type: $+; Schema: $+; Owner: $+",
                         section.name_prefix, section.name, section.`type`, section.schema, section.owner):
                    section.print_header(fh_stats_db)
                else:
                    dual_output(line.strip(trailing = false, chars = {'-', ' '}), fh_stats_db)

    section.print_stats(fh_stats_db)
    dual_output("", fh_stats_db)
    dual_output(fmt"Line count: {line_count}", fh_stats_db)
    dual_output(fmt"Section count: {section_count}", fh_stats_db)
